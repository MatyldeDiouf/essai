# Premier titre

## Premier sous-titre

### Premier sous-sous-titre

Markdown supporte le langage HTML &rightarrow; utile pour lettres grecques (*Ex:* &Sigma; ou subscript/superscript avec X<sup>2</sup>)

Trait horizontal:
---

Line break avec antislash \\ ou balise <br/>

Déclaration d'un commentaire avec : <!-- ce qui est écrit au sein de la balise ne sera pas formaté -->

Texte en **gras** ou __gras__

Texte en *italique* ou _italique_

Texte ~~barré~~

Texte en `chasse fixe` 

Précision du langage de programmation éventuellement:
```python
print("Hello world!")
```

Lien hypertexte: [Élaboration et conversion de documents avec Markdown et Pandoc](https://enacit.epfl.ch/cours/markdown-pandoc/)

Insertion d'une image avec: <br/>
![Légende de la photo](https://www.redeyedtreefrog.org/wp-content/uploads/2009/11/frog-tank-decoration.jpg "grenouille verte") <br/>
l'adresse de la photo pouvant être un lien internet ("copy image adress") ou un lien vers un fichier de ma machine

Construction d'une liste à puce à l'aide des caractères - ou + ou * : <br/>

- 1er élément
- 2nd élément
- 3e élément

Construction d'une énumération avec nombre suivi d'un point: <br/>

1. 1er élément
2. 2nd élément
3. 3e élément

On peut également imbriquer les listes à puces et énumération (utilisation d'indentations): <br/>

1. Première section
    - 1er élément
    - 2nd élément
2. Seconde section
    - 1er élément
    - 2nd élément
<!-- ajouter un saut de ligne avant la liste peut aider à l'affichage correct de la liste lors du formatage en pdf -->

Pour insérer une citation: As Kanye West said,

> Roses are red, violets are blue, I love Kanye and so do you.

$\alpha$ $\beta$ $\gamma$ $\pi$

Formules mathématiques:

* Mode "inline" (inclusion directe dans le texte, 1 \$):

$\sum_{i=1}^n X_i$

* Mode "displayed" (mise en exergue, 2 \$\$):

$$\sum_{i=1}^n X_i$$

Création de tables: <br/>

First Header | Second Header
-------------|--------------
First element | Second element
Third element | Fourth element


